<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Category;
use Auth;
use App\Friend;
use App\User;
class AddController extends Controller
{
    public function addCategory(Request $request)
    {
        $cates = Category::where('category','=',$request->text)->where('user_id','=',Auth::id())->get();
        if($cates->isEmpty())
        {
            $categories = new Category();
            $categories->category = $request->text;
            $categories->user_id = Auth::id();
            $categories->save();            
            return 'done';
        }
        else
            return 'failed';
    }
    public function addUser(Request $request)
    {
        $users = User::where('username','=',$request->friend)->where('user_id','=',Auth::id())->get();
        if($users->isEmpty())
        {
            $user = new User();
            $user->username = $request->friend;
            $user->user_id = Auth::id();
            $user->save();
            return 'done';
        }
        else
            return 'failed';
    }
}

<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Category;
use App\Transaction;
use View;
use Illuminate\Support\Facades\Auth;
use Session;
use App\User;
use App\Friend;
class HomeController extends Controller
{
    public function welcome()
    {
        return view('welcome');
    }
    public function index()
    {
        $id = Auth::id();
        $users = User::where('user_id','=',$id)->get();
        $categories = Category::where('user_id','=',Auth::id())->get();
        return View::make('home',compact('categories','users'));
    }
    public function store(Request $request)
    { 
        $paidby = $request->paidby;
        $category = $request->category;
       $paidby_auth = $request->paidby_auth;
        if(!isset($paidby_auth))
        {
        if($paidby == "Select user" )
        {
            Session::flash('inv_user','Please select valid "Paiby" option');
            return redirect()->back()->withInput();
        }
        }
        if($category == "Select Category")
        {
           Session::flash('inv_cat','Please select valid "Category"');
           return redirect()->back()->withInput();   
       }
       if($paidby_auth == null)
       {
        $validator = Validator($request->all(),[
            'amount' => 'required|numeric',
            'category' => 'required',
            'paidby' => 'required',
            'date' => 'required',
            ]);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $trans = Transaction::where('amount','=',$request->amount)->where('category_id','=',$request->category)->where('user_id','=',$request->paidby)->where('addedby_id','=',Auth::id())->where('dateOfEntry','=',$request->date)->where('description','=',$request->description)->get();
        if($trans->isEmpty())
        {
            $transactions = new Transaction();
            $transactions->amount = $request->amount;
            $transactions->category_id = $request->category;
            $transactions->user_id = $request->paidby;
            $transactions->addedby_id = Auth::id();
            $transactions->dateOfEntry = $request->date;
            $transactions->description = $request->description;
            $transactions->save();
            Session::flash('trans_success_msg','The transaction is succcessfully added');
            return redirect('/home');
        }
        else
        {
            Session::flash('trans_fail_msg','This transaction is already Existed');
            return redirect('/home');
        }
    }else
    {
        $validator = Validator($request->all(),[
            'amount' => 'required|numeric',
            'category' => 'required',
            'paidby_auth' => 'required',
            'date' => 'required',
            ]);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $trans = Transaction::where('amount','=',$request->amount)->where('category_id','=',$request->category)->where('user_id','=',$request->paidby_auth)->where('addedby_id','=',Auth::id())->where('dateOfEntry','=',$request->date)->where('description','=',$request->description)->get();
        if($trans->isEmpty())
        {
            $transactions = new Transaction();
            $transactions->amount = $request->amount;
            $transactions->category_id = $request->category;
            $transactions->user_id = $request->paidby_auth;
            $transactions->addedby_id = Auth::id();
            $transactions->dateOfEntry = $request->date;
            $transactions->description = $request->description;
            $transactions->save();
            Session::flash('trans_success_msg','The transaction is succcessfully added');
            return redirect('/home');
        }
        else
        {
            Session::flash('trans_fail_msg','This transaction is already Existed');
            return redirect('/home');
        }
    }
}

}
